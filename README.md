<!--- some badges to display on the GitHub page -->
![Travis (.org)](https://img.shields.io/travis/debuglevel/pdfformfiller-microservice?label=Travis%20build)
![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/debuglevel/pdfformfiller-microservice?label=GitLab%20build)
![GitHub release (latest SemVer)](https://img.shields.io/github/v/release/debuglevel/pdfformfiller-microservice?sort=semver)
![GitHub](https://img.shields.io/github/license/debuglevel/pdfformfiller-microservice)

# PDF form microservice

This is a microservice to fill and get values from PDF forms.

# HTTP API

## Swagger / OpenAPI

There is an OpenAPI (former: Swagger) specification created, which is available
at <http://localhost:8080/swagger/pdfformfiller-microservice-0.0.1.yml> (or somewhere in the jar file). It can easily be
pasted into the [Swagger Editor](https://editor.swagger.io) which provides a live demo
for [Swagger UI](https://swagger.io/tools/swagger-ui/), but also offers to create client libraries
via [Swagger Codegen](https://swagger.io/tools/swagger-codegen/).

## Forms

### Add form

PDF forms must be uploaded first in order to retrieve their fields, values or to use them as template for merging.

```json
{
  "pdfBase64": "JVBERi0xLjQKJcOkw7zDtsOfCjIgMCBvYmoKP[...]"
}
```

```shell script
$ curl --request POST -d @upload.json -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8080/forms/

{
  "id" : "e103c112-a69e-400d-8d2f-6c1732cbb608"
}
```

This returns the `id` of this added form.

### Get form

This ID can be used to retrieve information from a form (fields, values, Base64 encoded PDF content) or to reference it
as a template form in a merge request.

```bash
$ curl --request GET 'http://localhost:8080/forms/e103c112-a69e-400d-8d2f-6c1732cbb608'

{
    "id": "e103c112-a69e-400d-8d2f-6c1732cbb608",
    "pdfBase64": "JVBERi0xLjYKJcOkw7zDtsOf[...]",
    "values": {
        "Field 1": "",
        "Field 2": "",
        "Field 3": ""
    },
    "createdOn": "2021-04-10T17:45:55.544",
    "lastModifiedOn": "2021-04-10T17:45:55.544"
}
```

This form has 3 fields with no content.

### Get merged form

A form might also be the result of a merge request. Those can also be retrieved at this endpoint. In this case, the
fields would probably have values (or no fields left, when they were removed due to `"flatten": true`):

```bash
curl --location --request GET 'http://localhost:8080/forms/2a372445-e878-49ab-b61a-874388a1018f'

{
    "id": "2a372445-e878-49ab-b61a-874388a1018f",
    "pdfBase64": "JVBERi0xLjYKJeLjz[...]",
    "values": {
        "Field 1": "Filled1",
        "Field 2": "Filled2",
        "Field 3": ""
    },
    "createdOn": "2021-04-10T17:55:20.596",
    "lastModifiedOn": "2021-04-10T17:55:20.596"
}
```

### List forms

All available forms can be listed:

```shell script
$ curl -X GET -H "Accept: application/json" http://localhost:8080/forms/

[
    {
        "id": "e103c112-a69e-400d-8d2f-6c1732cbb608",
        "createdOn": "2021-04-10T17:45:55.544",
        "lastModifiedOn": "2021-04-10T17:45:55.544"
    },
    {
        "id": "2a372445-e878-49ab-b61a-874388a1018f",
        "createdOn": "2021-04-10T17:55:20.596",
        "lastModifiedOn": "2021-04-10T17:55:20.596"
    }
]
```

## Merge

### Add merge

When a template form exists, a merge can be requested. The `id` of the template form is referenced as `templateFormId`:

````bash
$ curl --request POST 'http://localhost:8080/merges' --header 'Content-Type: application/json' --data-raw \
'{
    "templateFormId": "e103c112-a69e-400d-8d2f-6c1732cbb608",
    "values": {
        "Field 1": "Filled1",
        "Field 2": "Filled2"
    }
}'

{
    "id": "1b5e2064-3955-40e6-807d-7ffa286063d9",
    "templateFormId": "e103c112-a69e-400d-8d2f-6c1732cbb608"
}
````

The service enqueues the merge (which usually should be done really fast) and returns the `id` of the merge request.

### Get merge

To get the resulting merge (or just to have a look again at the merge request), request the merge with its `id`:

```bash
$ curl --request GET 'http://localhost:8080/merges/1b5e2064-3955-40e6-807d-7ffa286063d9'

{
    "id": "1b5e2064-3955-40e6-807d-7ffa286063d9",
    "templateFormId": "e103c112-a69e-400d-8d2f-6c1732cbb608",
    "mergedFormId": "2a372445-e878-49ab-b61a-874388a1018f",
    "flatten": false,
    "createdOn": "2021-04-10T17:55:20.165",
    "lastModifiedOn": "2021-04-10T17:55:20.723"
}
```

The resulting merged form was saved as `mergedFormId` and can be retrieved from the `/forms/` endpoint.

In this case, the request to `/merges` would include the `id` rather than the Base64 encoded PDF.

# Configuration

There is a `application.yml` included in the jar file. Its content can be modified and saved as a
separate `application.yml` on the level of the jar file. Configuration can also be applied via the other supported ways
of Micronaut (see <https://docs.micronaut.io/latest/guide/index.html#config>). For Docker, the configuration via
environment variables is the most interesting one (see `docker-compose.yml`).
