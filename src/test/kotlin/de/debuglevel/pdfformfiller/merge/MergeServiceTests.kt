package de.debuglevel.pdfformfiller.merge

import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.within
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.*
import javax.inject.Inject
import kotlin.io.path.ExperimentalPathApi

@ExperimentalPathApi
@MicronautTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MergeServiceTests {
    @Inject
    lateinit var service: MergeService

//    fun mergeProvider() = TestDataProvider.mergeProvider()
//    fun filledMergeProvider() = TestDataProvider.filledMergeProvider()

    @Test
    fun `get merge`() {
        // Arrange
        val merge = Merge(
            id = null,
            templateFormId = UUID.randomUUID(),
            mergedFormId = UUID.randomUUID(),
            flatten = false,
        )
        val addedMerge = service.add(merge)

        // Act
        val retrievedMerge = service.get(addedMerge.id!!)

        // Assert
        assertThat(retrievedMerge.id).isNotNull
        assertThat(retrievedMerge.templateFormId).isEqualTo(merge.templateFormId)
        assertThat(retrievedMerge.mergedFormId).isEqualTo(merge.mergedFormId)
        assertThat(retrievedMerge.flatten).isEqualTo(merge.flatten)
        assertThat(retrievedMerge.createdOn).isNotNull
        assertThat(retrievedMerge.lastModifiedOn).isNotNull
    }

    @Test
    fun `get all merges`() {
        // Arrange
        service.deleteAll()

        for (iteration in 1..10) {
            // Arrange
            val merge = Merge(
                id = null,
                templateFormId = UUID.randomUUID(),
                mergedFormId = UUID.randomUUID(),
                flatten = false,
            )
            val addedMerge = service.add(merge)

            // Act
            val retrievedMerges = service.getList()

            // Assert
            assertThat(retrievedMerges.size).isEqualTo(iteration)
            assertThat(retrievedMerges).allSatisfy { assertThat(it.id).isNotNull() }
            assertThat(retrievedMerges).allSatisfy { assertThat(it.createdOn).isNotNull() }
            assertThat(retrievedMerges).allSatisfy { assertThat(it.lastModifiedOn).isNotNull() }
        }
    }

    @Test
    fun `add merge`() {
        // Arrange

        // Act
        val merge = Merge(
            id = null,
            templateFormId = UUID.randomUUID(),
            mergedFormId = UUID.randomUUID(),
            flatten = false,
        )
        val addedMerge = service.add(merge)

        // Assert
        val retrievedMerge = service.get(addedMerge.id!!)
        setOf(addedMerge, retrievedMerge).forEach {
            assertThat(it.id).isNotNull
            assertThat(it.templateFormId).isEqualTo(merge.templateFormId)
            assertThat(it.mergedFormId).isEqualTo(merge.mergedFormId)
            assertThat(it.flatten).isEqualTo(merge.flatten)
            assertThat(it.createdOn).isCloseTo(LocalDateTime.now(), within(2, ChronoUnit.SECONDS))
            assertThat(it.lastModifiedOn).isCloseTo(LocalDateTime.now(), within(2, ChronoUnit.SECONDS))
        }
    }

    @Test
    fun `delete merge`() {
        // Arrange
        service.deleteAll()
        val merge = Merge(
            id = null,
            templateFormId = UUID.randomUUID(),
            mergedFormId = UUID.randomUUID(),
            flatten = false,
        )
        val addedMerge = service.add(merge)

        // Act
        service.delete(addedMerge.id!!)

        // Assert
        assertThat(service.getList().size).isEqualTo(0)
    }

    @Test
    fun `delete all merges`() {
        // Arrange
        service.deleteAll()

        for (iteration in 1..10) {
            // Arrange
            val merge = Merge(
                id = null,
                templateFormId = UUID.randomUUID(),
                mergedFormId = UUID.randomUUID(),
                flatten = false,
            )
            val addedMerge = service.add(merge)
        }

        // Act
        service.deleteAll()

        // Assert
        assertThat(service.getList().size).isEqualTo(0)
    }

    @Test
    fun `update merge`() {
        // Arrange
        val merge = Merge(
            id = null,
            templateFormId = UUID.randomUUID(),
            mergedFormId = UUID.randomUUID(),
            flatten = false,
        )
        val addedMerge = service.add(merge)

        val retrievedMerge = service.get(addedMerge.id!!)

        // Act
        retrievedMerge.templateFormId = UUID.randomUUID()
        retrievedMerge.mergedFormId = UUID.randomUUID()
        retrievedMerge.flatten = true
        val updatedMerge = service.update(retrievedMerge.id!!, retrievedMerge)

        // Assert
        val retrieved2Merge = service.get(addedMerge.id!!)
        setOf(updatedMerge, retrieved2Merge).forEach {
            assertThat(it.id).isNotNull
            assertThat(it.templateFormId).isEqualTo(retrievedMerge.templateFormId) // TODO: maybe this should not be possible, as it does not really makes sense
            assertThat(it.mergedFormId).isEqualTo(retrievedMerge.mergedFormId)
            assertThat(it.flatten).isEqualTo(retrievedMerge.flatten)
            assertThat(it.createdOn).isCloseTo(LocalDateTime.now(), within(2, ChronoUnit.SECONDS))
            assertThat(it.lastModifiedOn).isCloseTo(LocalDateTime.now(), within(2, ChronoUnit.SECONDS))
            assertThat(it.lastModifiedOn).isAfter(retrievedMerge.createdOn)
        }
    }
}