package de.debuglevel.pdfformfiller.pdf

import java.nio.file.Path
import java.nio.file.Paths
import java.util.stream.Stream

object TestDataProvider {
    /**
     * @param pdfPath Path to the PDF form
     * @param expectedFields Fields to be expected in the PDF
     */
    data class TestForm(
        val pdfPath: Path,
        val expectedFields: Set<String>
    )

    fun formProvider() = Stream.of(
        TestForm(
            Paths.get("example/forms/PDF form FDF.pdf"),
            setOf(
                "Field 1",
                "Field 2"
            )
        ),
        TestForm(
            Paths.get("example/forms/PDF form HTML.pdf"),
            setOf(
                "Field 1",
                "Field 2"
            )
        ),
        TestForm(
            Paths.get("example/forms/PDF form PDF PDF-A.pdf"),
            setOf(
                "Field 1",
                "Field 2"
            )
        ),
        TestForm(
            Paths.get("example/forms/PDF form PDF.pdf"),
            setOf(
                "Field 1",
                "Field 2"
            )
        ),
        TestForm(
            Paths.get("example/forms/PDF form XML.pdf"),
            setOf(
                "Field 1",
                "Field 2"
            )
        ),
    )

    /**
     * @param pdfPath Path to the PDF form
     * @param expectedFieldValues Fields and their values to be expected in the PDF
     */
    data class TestFilledForm(
        val pdfPath: Path,
        val expectedFieldValues: Map<String, String>
    )

    fun filledFormProvider() = Stream.of(
        TestFilledForm(
            Paths.get("example/values/PDF form FDF.pdf"),
            mapOf(
                "Field 1" to "Filled1",
                "Field 2" to "Filled2"
            )
        ),
        TestFilledForm(
            Paths.get("example/values/PDF form HTML.pdf"),
            mapOf(
                "Field 1" to "Filled1",
                "Field 2" to "Filled2"
            )
        ),
        TestFilledForm(
            Paths.get("example/values/PDF form PDF PDF-A.pdf"),
            mapOf(
                "Field 1" to "Filled1",
                "Field 2" to "Filled2"
            )
        ),
        TestFilledForm(
            Paths.get("example/values/PDF form PDF.pdf"),
            mapOf(
                "Field 1" to "Filled1",
                "Field 2" to "Filled2"
            )
        ),
        TestFilledForm(
            Paths.get("example/values/PDF form XML.pdf"),
            mapOf(
                "Field 1" to "Filled1",
                "Field 2" to "Filled2"
            )
        ),
    )
}