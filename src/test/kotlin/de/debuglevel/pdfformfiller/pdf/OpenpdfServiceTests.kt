package de.debuglevel.pdfformfiller.pdf

import com.lowagie.text.pdf.PdfReader
import com.lowagie.text.pdf.parser.PdfTextExtractor
import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.io.ByteArrayOutputStream
import javax.inject.Inject
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.inputStream

@ExperimentalPathApi
@MicronautTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class OpenpdfServiceTests {
    @Inject
    lateinit var service: PdfService

    fun formProvider() = TestDataProvider.formProvider()
    fun filledFormProvider() = TestDataProvider.filledFormProvider()

    @ParameterizedTest
    @MethodSource("formProvider")
    fun `get fields`(testForm: TestDataProvider.TestForm) {
        // Arrange
        val inputStream = testForm.pdfPath.inputStream()

        // Act
        val fields = service.getFields(inputStream)

        // Assert
        Assertions.assertThat(fields).containsExactlyInAnyOrder(*testForm.expectedFields.toTypedArray())
    }

    @ParameterizedTest
    @MethodSource("filledFormProvider")
    fun `get fields and values`(testFilledForm: TestDataProvider.TestFilledForm) {
        // Arrange
        val inputStream = testFilledForm.pdfPath.toFile().inputStream()

        // Act
        val values = service.getFieldValues(inputStream)

        // Assert
        Assertions.assertThat(values).containsExactlyInAnyOrderEntriesOf(testFilledForm.expectedFieldValues)
    }

    @ParameterizedTest
    @MethodSource("formProvider")
    fun `validate valid PDFs`(testForm: TestDataProvider.TestForm) {
        // Arrange
        val inputStream = testForm.pdfPath.toFile().inputStream()

        // Act
        val isValid = service.validate(inputStream)

        // Assert
        Assertions.assertThat(isValid).isTrue
    }

    @Test
    fun `validate invalid PDF`() {
        // Arrange
        val inputStream = "foobar".byteInputStream()

        // Act
        val isValid = service.validate(inputStream)

        // Assert
        Assertions.assertThat(isValid).isFalse
    }

    @ParameterizedTest
    @MethodSource("formProvider")
    fun `flattening merge values with form`(testForm: TestDataProvider.TestForm) {
        // Arrange
        val inputStream = testForm.pdfPath.toFile().inputStream()
        val values = testForm.expectedFields.associateWith { "$it Value" }

        val outputStream = ByteArrayOutputStream()
        val flatten = true // PdfTextExtractor does not find text without flattening

        // Act
        service.merge(inputStream, values, outputStream, flatten)

        // Assert
        val bytes = outputStream.toByteArray()
        val text = PdfTextExtractor(PdfReader(bytes)).getTextFromPage(1)
        Assertions.assertThat(text).contains(values.values)
    }

    @ParameterizedTest
    @MethodSource("formProvider")
    fun `non-flattening merge values with form`(testForm: TestDataProvider.TestForm) {
        // Arrange
        val inputStream = testForm.pdfPath.toFile().inputStream()
        val values = testForm.expectedFields.associateWith { "$it Value" }

        val outputStream = ByteArrayOutputStream()
        val flatten = false

        // Act
        service.merge(inputStream, values, outputStream, flatten)

        // Assert
        val bytes = outputStream.toByteArray()
        val values2 = service.getFieldValues(bytes.inputStream())
        Assertions.assertThat(values2).containsExactlyInAnyOrderEntriesOf(values)
    }
}