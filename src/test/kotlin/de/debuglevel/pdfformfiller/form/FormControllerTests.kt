package de.debuglevel.pdfformfiller.form

import de.debuglevel.pdfformfiller.PdfUtils
import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.util.*
import javax.inject.Inject

@MicronautTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FormControllerTests {

    @Inject
    lateinit var controller: FormController

    @Inject
    lateinit var service: FormService

    @Test
    fun `updating an item works at all`() {
        //println("'${PdfUtils.getMinimalPdf()}'")

        // Arrange
        val minimalPdfBase64 = Base64.getEncoder().encodeToString(PdfUtils.getMinimalPdf().toByteArray())
        val form =
            AddFormRequest(
                pdfBase64 = minimalPdfBase64
            )
        val addedForm = controller.postOne(form).body() as AddFormResponse

        // Act
        val retrievedForm = controller.getOne(addedForm.id).body() as GetFormResponse

        val updatedPdfBase64 =
            Base64.getEncoder().encodeToString(PdfUtils.getMinimalPdf().toByteArray().plus("_updated".toByteArray()))
        val updateForm = UpdateFormRequest(
            pdfBase64 = updatedPdfBase64
        )

        val updatedForm = controller.putOne(retrievedForm.id, updateForm).body() as UpdateFormResponse

        // Assert
        val retrieved2Form = controller.getOne(updatedForm.id).body() as GetFormResponse
        assertThat(retrieved2Form.pdfBase64).isEqualTo(updatedPdfBase64)
    }
}