package de.debuglevel.pdfformfiller.form

import de.debuglevel.pdfformfiller.PdfUtils
import de.debuglevel.pdfformfiller.pdf.TestDataProvider
import de.debuglevel.pdfformfiller.withResponseOnException
import io.micronaut.http.BasicAuth
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.*
import javax.inject.Inject
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.readBytes

@ExperimentalPathApi
@MicronautTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FormClientTests {
    fun formProvider() = TestDataProvider.formProvider()
    fun filledFormProvider() = TestDataProvider.filledFormProvider()

    private val basicAuth = BasicAuth("username", "password")

    @Inject
    lateinit var formClient: FormClient

    @Test
    fun `get form`() {
        // Arrange
        val minimalPdfBytes = PdfUtils.getMinimalPdf().toByteArray()
        val addFormRequest = AddFormRequest(
            pdfBase64 = Base64.getEncoder().encodeToString(minimalPdfBytes)
        )
        val addFormResponse = formClient.postOne(addFormRequest).body()!!

        // Act
        val httpResponse = formClient.getOne(addFormResponse.id)

        // Assert
        assertThat(httpResponse.status).isEqualTo(HttpStatus.OK)
        assertThat(httpResponse.contentType.get()).isEqualTo(MediaType.APPLICATION_JSON_TYPE)
        assertThat(httpResponse.contentLength).isGreaterThan(0)

        val getFormResponse = httpResponse.body.get()
        assertThat(getFormResponse.id).isNotNull
        assertThat(Base64.getDecoder().decode(getFormResponse.pdfBase64)).isEqualTo(minimalPdfBytes)
        assertThat(getFormResponse.createdOn).isNotNull
        assertThat(getFormResponse.lastModifiedOn).isNotNull
    }


    @Test
    fun `get form with an unknown id`() {
        // Arrange
        val unknownId = UUID.randomUUID()

        // Act
        val httpResponse = formClient.getOne(unknownId)

        // Assert
        assertThat(httpResponse.status).isEqualTo(HttpStatus.NOT_FOUND)
    }

    @Test
    fun `get all forms`() {
        // Arrange
        formClient.deleteAll(basicAuth)

        for (iteration in 1..10) {
            // Arrange
            val minimalPdfBytes = PdfUtils.getMinimalPdf().toByteArray()
            val addFormRequest = AddFormRequest(
                pdfBase64 = Base64.getEncoder().encodeToString(minimalPdfBytes)
            )
            val addFormResponse = formClient.postOne(addFormRequest).body()!!

            // Act
            val httpResponse = formClient.getList(basicAuth)

            // Assert
            assertThat(httpResponse.status).isEqualTo(HttpStatus.OK)
            assertThat(httpResponse.contentType.get()).isEqualTo(MediaType.APPLICATION_JSON_TYPE)
            assertThat(httpResponse.contentLength).isGreaterThan(0)

            val getFormResponses = httpResponse.body.get()
            assertThat(getFormResponses.size).isEqualTo(iteration)
            assertThat(getFormResponses).allSatisfy { assertThat(it.id).isNotNull() }
            assertThat(getFormResponses).allSatisfy { assertThat(it.createdOn).isNotNull() }
            assertThat(getFormResponses).allSatisfy { assertThat(it.lastModifiedOn).isNotNull() }
        }
    }

    @Test
    fun `add form`() {
        // Arrange

        // Act
        val minimalPdfBytes = PdfUtils.getMinimalPdf().toByteArray()
        val addFormRequest = AddFormRequest(
            pdfBase64 = Base64.getEncoder().encodeToString(minimalPdfBytes)
        )
        val httpResponse = formClient.postOne(addFormRequest)
        val addFormResponse = httpResponse.body()!!

        // Assert
        assertThat(httpResponse.status).isEqualTo(HttpStatus.CREATED)
        assertThat(httpResponse.contentType.get()).isEqualTo(MediaType.APPLICATION_JSON_TYPE)
        assertThat(httpResponse.contentLength).isGreaterThan(0)
        assertThat(httpResponse.header("Location")).contains(addFormResponse.id.toString())

        val getFormResponse = formClient.getOne(addFormResponse.id).body.get()
        assertThat(addFormResponse.id).isNotNull
        assertThat(getFormResponse.id).isEqualTo(addFormResponse.id)
        assertThat(Base64.getDecoder().decode(getFormResponse.pdfBase64)).isEqualTo(minimalPdfBytes)
        assertThat(getFormResponse.createdOn).isCloseTo(LocalDateTime.now(), Assertions.within(2, ChronoUnit.SECONDS))
        assertThat(getFormResponse.lastModifiedOn).isCloseTo(
            LocalDateTime.now(),
            Assertions.within(2, ChronoUnit.SECONDS)
        )
    }

    @Test
    fun `add invalid form`() {
        // Arrange

        // Act
        val bytes = "foobar".toByteArray()
        val addFormRequest = AddFormRequest(
            pdfBase64 = Base64.getEncoder().encodeToString(bytes)
        )
        val httpResponse = withResponseOnException { formClient.postOne(addFormRequest) }

        // Assert
        assertThat(httpResponse.status).isEqualTo(HttpStatus.BAD_REQUEST)
    }

    @Test
    fun `delete form`() {
        // Arrange
        formClient.deleteAll(basicAuth)
        val minimalPdfBytes = PdfUtils.getMinimalPdf().toByteArray()
        val addFormRequest = AddFormRequest(
            pdfBase64 = Base64.getEncoder().encodeToString(minimalPdfBytes)
        )
        val addFormResponse = formClient.postOne(addFormRequest).body()!!

        // Act
        val httpResponse = formClient.deleteOne(addFormResponse.id)

        // Assert
        assertThat(httpResponse.status).isEqualTo(HttpStatus.NO_CONTENT)
        assertThat(httpResponse.contentLength).isEqualTo(-1)

        val getFormResponses = formClient.getList(basicAuth).body.get()
        assertThat(getFormResponses.size).isEqualTo(0)
    }

    @Test
    fun `delete form with unknown id`() {
        // Arrange
        val unknownId = UUID.randomUUID()

        // Act
        val httpResponse = formClient.deleteOne(unknownId)

        // Assert
        assertThat(httpResponse.status).isEqualTo(HttpStatus.NOT_FOUND)
    }

    @Test
    fun `delete all forms`() {
        // Arrange
        formClient.deleteAll(basicAuth)

        for (iteration in 1..10) {
            val minimalPdfBytes = PdfUtils.getMinimalPdf().toByteArray()
            val addFormRequest = AddFormRequest(
                pdfBase64 = Base64.getEncoder().encodeToString(minimalPdfBytes)
            )
            val addFormResponse = formClient.postOne(addFormRequest).body()!!
        }

        // Act
        val httpResponse = formClient.deleteAll(basicAuth)

        // Assert
        assertThat(httpResponse.status).isEqualTo(HttpStatus.NO_CONTENT)
        assertThat(httpResponse.contentLength).isEqualTo(-1)

        val getFormResponses = formClient.getList(basicAuth).body.get()
        assertThat(getFormResponses.size).isEqualTo(0)
    }

    @ParameterizedTest
    @MethodSource("filledFormProvider")
    fun `get fields and values`(testFilledForm: TestDataProvider.TestFilledForm) {
        // Arrange
        val bytes = testFilledForm.pdfPath.readBytes()
        val addFormRequest = AddFormRequest(
            pdfBase64 = Base64.getEncoder().encodeToString(bytes)
        )
        val addFormResponse = formClient.postOne(addFormRequest).body()!!

        // Act
        val httpResponse = formClient.getOne(addFormResponse.id)

        // Assert
        assertThat(httpResponse.status).isEqualTo(HttpStatus.OK)
        assertThat(httpResponse.contentType.get()).isEqualTo(MediaType.APPLICATION_JSON_TYPE)
        assertThat(httpResponse.contentLength).isGreaterThan(0)

        val getFormResponse = httpResponse.body.get()
        assertThat(getFormResponse.id).isNotNull
        assertThat(Base64.getDecoder().decode(getFormResponse.pdfBase64)).isEqualTo(bytes)
        assertThat(getFormResponse.values).containsExactlyInAnyOrderEntriesOf(testFilledForm.expectedFieldValues)
        assertThat(getFormResponse.createdOn).isNotNull
        assertThat(getFormResponse.lastModifiedOn).isNotNull
    }

//    @Test
//    fun `update form`() {
//        // Arrange
//        val minimalPdfBytes = PdfUtils.getMinimalPdf().toByteArray()
//        val form = Form(id = null, pdf = minimalPdfBytes)
//        val addedForm = service.add(form)
//
//        val retrievedForm = service.get(addedForm.id!!)
//
//        // Act
//        val updatedPdfBytes = minimalPdfBytes.plus("updated".toByteArray())
//        retrievedForm.pdf = updatedPdfBytes
//        val updatedForm = service.update(retrievedForm.id!!, retrievedForm)
//
//        // Assert
//        val retrieved2Form = service.get(addedForm.id!!)
//        setOf(updatedForm, retrieved2Form).forEach {
//            assertThat(it.id).isNotNull
//            assertThat(it.pdf).isEqualTo(updatedPdfBytes)
//            assertThat(it.createdOn).isCloseTo(LocalDateTime.now(), Assertions.within(2, ChronoUnit.SECONDS))
//            assertThat(it.lastModifiedOn).isCloseTo(LocalDateTime.now(), Assertions.within(2, ChronoUnit.SECONDS))
//            assertThat(it.lastModifiedOn).isAfter(retrievedForm.createdOn)
//        }
//    }

//    @Test
//    fun `update form with unknown id`() {
//        // Arrange
//        val unknownId = UUID.randomUUID()
//
//        val form = Form(
//            id = unknownId,
//            pdf = PdfUtils.getMinimalPdf().toByteArray(),
//        )
//
//        // Act
//        val exception = assertThrows<FormService.FormNotFoundException> { service.update(unknownId, form) }
//    }
}