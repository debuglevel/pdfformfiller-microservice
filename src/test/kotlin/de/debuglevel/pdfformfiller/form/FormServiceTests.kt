package de.debuglevel.pdfformfiller.form

import de.debuglevel.pdfformfiller.PdfUtils
import de.debuglevel.pdfformfiller.pdf.TestDataProvider
import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.within
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.*
import javax.inject.Inject
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.readBytes

@ExperimentalPathApi
@MicronautTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FormServiceTests {
    @Inject
    lateinit var service: FormService

    fun formProvider() = TestDataProvider.formProvider()
    fun filledFormProvider() = TestDataProvider.filledFormProvider()

    @Test
    fun `get form`() {
        // Arrange
        val minimalPdfBytes = PdfUtils.getMinimalPdf().toByteArray()
        val form = Form(id = null, pdf = minimalPdfBytes)
        val addedForm = service.add(form)

        // Act
        val retrievedForm = service.get(addedForm.id!!)

        // Assert
        assertThat(retrievedForm.id).isNotNull
        assertThat(retrievedForm.pdf).isEqualTo(minimalPdfBytes)
        assertThat(retrievedForm.createdOn).isNotNull
        assertThat(retrievedForm.lastModifiedOn).isNotNull
    }

    @Test
    fun `get form with an unknown id`() {
        // Arrange
        val unknownId = UUID.randomUUID()

        // Act & Assert
        val exception = assertThrows<FormService.FormNotFoundException> { service.get(unknownId) }
    }

    @Test
    fun `get all forms`() {
        // Arrange
        service.deleteAll()

        for (iteration in 1..10) {
            // Arrange
            val minimalPdfBytes = PdfUtils.getMinimalPdf().toByteArray()
            val form = Form(id = null, pdf = minimalPdfBytes)
            val addedForm = service.add(form)

            // Act
            val retrievedForms = service.getList()

            // Assert
            assertThat(retrievedForms.size).isEqualTo(iteration)
            assertThat(retrievedForms).allSatisfy { assertThat(it.id).isNotNull() }
            assertThat(retrievedForms).allSatisfy { assertThat(it.createdOn).isNotNull() }
            assertThat(retrievedForms).allSatisfy { assertThat(it.lastModifiedOn).isNotNull() }
        }
    }

    @Test
    fun `add form`() {
        // Arrange

        // Act
        val minimalPdfBytes = PdfUtils.getMinimalPdf().toByteArray()
        val form = Form(id = null, pdf = minimalPdfBytes)
        val addedForm = service.add(form)

        // Assert
        val retrievedForm = service.get(addedForm.id!!)
        setOf(addedForm, retrievedForm).forEach {
            assertThat(it.id).isNotNull
            assertThat(it.pdf).isEqualTo(minimalPdfBytes)
            assertThat(it.createdOn).isCloseTo(LocalDateTime.now(), within(2, ChronoUnit.SECONDS))
            assertThat(it.lastModifiedOn).isCloseTo(LocalDateTime.now(), within(2, ChronoUnit.SECONDS))
        }
    }

    @Test
    fun `add invalid form`() {
        // Arrange

        // Act & Assert
        val bytes = "foobar".toByteArray()
        val form = Form(id = null, pdf = bytes)
        val exception = assertThrows<FormService.InvalidPdfException> { service.add(form) }
    }

    @Test
    fun `delete form`() {
        // Arrange
        service.deleteAll()
        val minimalPdfBytes = PdfUtils.getMinimalPdf().toByteArray()
        val form = Form(id = null, pdf = minimalPdfBytes)
        val addedForm = service.add(form)

        // Act
        service.delete(addedForm.id!!)

        // Assert
        assertThat(service.getList().size).isEqualTo(0)
    }

    @Test
    fun `delete form with unknown id`() {
        // Arrange
        val unknownId = UUID.randomUUID()

        // Act & Assert
        val exception = assertThrows<FormService.FormNotFoundException> { service.delete(unknownId) }
    }

    @Test
    fun `delete all forms`() {
        // Arrange
        service.deleteAll()

        for (iteration in 1..10) {
            // Arrange
            val minimalPdfBytes = PdfUtils.getMinimalPdf().toByteArray()
            val form = Form(id = null, pdf = minimalPdfBytes)
            val addedForm = service.add(form)
        }

        // Act
        service.deleteAll()

        // Assert
        assertThat(service.getList().size).isEqualTo(0)
    }

    @ParameterizedTest
    @MethodSource("filledFormProvider")
    fun `get fields and values`(testFilledForm: TestDataProvider.TestFilledForm) {
        // Arrange
        val bytes = testFilledForm.pdfPath.readBytes()
        val form = Form(id = null, pdf = bytes)
        val addedForm = service.add(form)

        // Act
        val fieldValues = service.getFieldValues(addedForm.id!!)

        // Assert
        assertThat(fieldValues).containsExactlyInAnyOrderEntriesOf(testFilledForm.expectedFieldValues)
    }

    @Test
    fun `get fields and values with an unknown form id`() {
        // Arrange
        val unknownId = UUID.randomUUID()

        // Act
        val exception = assertThrows<FormService.FormNotFoundException> { service.getFieldValues(unknownId) }
    }

    @Test
    fun `update form`() {
        // Arrange
        val minimalPdfBytes = PdfUtils.getMinimalPdf().toByteArray()
        val form = Form(id = null, pdf = minimalPdfBytes)
        val addedForm = service.add(form)

        val retrievedForm = service.get(addedForm.id!!)

        // Act
        val updatedPdfBytes = minimalPdfBytes.plus("updated".toByteArray())
        retrievedForm.pdf = updatedPdfBytes
        val updatedForm = service.update(retrievedForm.id!!, retrievedForm)

        // Assert
        val retrieved2Form = service.get(addedForm.id!!)
        setOf(updatedForm, retrieved2Form).forEach {
            assertThat(it.id).isNotNull
            assertThat(it.pdf).isEqualTo(updatedPdfBytes)
            assertThat(it.createdOn).isCloseTo(LocalDateTime.now(), within(2, ChronoUnit.SECONDS))
            assertThat(it.lastModifiedOn).isCloseTo(LocalDateTime.now(), within(2, ChronoUnit.SECONDS))
            assertThat(it.lastModifiedOn).isAfter(retrievedForm.createdOn)
        }
    }

    @Test
    fun `update form with unknown id`() {
        // Arrange
        val unknownId = UUID.randomUUID()

        val form = Form(
            id = unknownId,
            pdf = PdfUtils.getMinimalPdf().toByteArray(),
        )

        // Act
        val exception = assertThrows<FormService.FormNotFoundException> { service.update(unknownId, form) }
    }
}