package de.debuglevel.pdfformfiller.form

import io.micronaut.http.BasicAuth
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.*
import io.micronaut.http.client.annotation.Client
import java.util.*
import javax.validation.constraints.NotBlank

@Client("/forms")
interface FormClient {
    @Get("/")
    fun getList(basicAuth: BasicAuth): HttpResponse<Set<GetFormResponse>>

    @Get("/{id}")
    fun getOne(@NotBlank id: UUID): HttpResponse<GetFormResponse>

    @Put("/{id}")
    fun putOne(@NotBlank id: UUID, @Body updateFormRequest: UpdateFormRequest): HttpResponse<UpdateFormResponse>

    @Post("/")
    fun postOne(@Body addFormRequest: AddFormRequest): HttpResponse<AddFormResponse>

    @Delete("/{id}")
    fun deleteOne(@NotBlank id: UUID): HttpResponse<Unit>

    @Delete("/")
    fun deleteAll(basicAuth: BasicAuth): HttpResponse<Unit>
}