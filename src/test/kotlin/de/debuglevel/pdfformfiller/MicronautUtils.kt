package de.debuglevel.pdfformfiller

import io.micronaut.context.ApplicationContext
import io.micronaut.http.HttpResponse
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.runtime.server.EmbeddedServer

object MicronautUtils {
    fun getPort(applicationContext: ApplicationContext): Int {
        return applicationContext.getBean(EmbeddedServer::class.java).port
    }
}

fun withResponseOnException(block: () -> HttpResponse<*>): HttpResponse<*> {
    return try {
        return block()
    } catch (e: HttpClientResponseException) {
        e.response
    }
}