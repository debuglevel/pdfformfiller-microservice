package de.debuglevel.pdfformfiller.pdf

import java.io.InputStream
import java.io.OutputStream

interface PdfService {
    /**
     * Fills in the values in their fields.
     * @param templateForm the PDF form to be filled
     * @param values the values to be filled in the form (where the key is the field name and the value is the value to fill in)
     * @param mergedForm the modified PDF with the values filled in
     * @param flatten true if the form controls should be removed (i.e. the PDF becomes read only)
     */
    fun merge(
        templateForm: InputStream,
        values: Map<String, String>,
        mergedForm: OutputStream,
        flatten: Boolean = true
    )

    /**
     * Validates whether an InputStream is a valid PDF.
     * @return True if valid PDF
     */
    fun validate(pdf: InputStream): Boolean

    /**
     * Gets all field names of a PDF form.
     * @param pdfForm the PDF form to get the field names from
     * @return Field names in the PDF from
     */
    fun getFields(pdfForm: InputStream): Set<String>

    /**
     * Gets all fields and their values.
     * @param filledPdfForm the filled PDF form to get the fields and values from.
     * @return Map of the field names and their values.
     */
    fun getFieldValues(filledPdfForm: InputStream): Map<String, String>
}