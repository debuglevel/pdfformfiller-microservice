package de.debuglevel.pdfformfiller.pdf.openpdf

import com.lowagie.text.pdf.PdfReader
import com.lowagie.text.pdf.PdfStamper
import de.debuglevel.pdfformfiller.pdf.PdfService
import mu.KotlinLogging
import java.io.InputStream
import java.io.OutputStream
import javax.inject.Singleton

@Singleton
class OpenpdfService : PdfService {
    private val logger = KotlinLogging.logger {}

    /**
     * @implNote: Probably not the very best PDF validation check, but should catch the worst cases
     */
    override fun validate(pdf: InputStream): Boolean {
        logger.debug { "Validating PDF..." }

        val isValidPdf = try {
            val pdfReader = PdfReader(pdf)
            val isValid = pdfReader.numberOfPages > 0
            logger.debug { "Parsing PDF worked with ${pdfReader.numberOfPages} pages; assuming PDF is valid: $isValid" }
            isValid
        } catch (e: Exception) {
            logger.debug { "Parsing PDF failed; assuming PDF is not valid: ${e.stackTrace}" }
            false
        }

        logger.debug { "Validated PDF: $isValidPdf" }
        return isValidPdf
    }

    override fun merge(
        templateForm: InputStream,
        values: Map<String, String>,
        mergedForm: OutputStream,
        flatten: Boolean
    ) {
        logger.debug { "Merging PDF..." }

        val pdfReader = PdfReader(templateForm)
        val pdfStamper = PdfStamper(pdfReader, mergedForm)

        // fill in form values
        val form = pdfStamper.acroFields
        values.forEach { (field, value) ->
            run {
                logger.debug { "Setting field '$field'='$value'" }
                form.setField(field, value)
            }
        }

        // remove form controls and only retain the values
        pdfStamper.setFormFlattening(flatten)

        pdfStamper.close()
        pdfReader.close()

        logger.debug { "Merged PDF" }
    }

    override fun getFields(pdfForm: InputStream): Set<String> {
        logger.debug { "Getting fields in PDF..." }

        val pdfReader = PdfReader(pdfForm)
        val fields = getFields(pdfReader)
        pdfReader.close()

        logger.debug { "Got fields in PDF: $fields" }
        return fields
    }

    private fun getFields(pdfReader: PdfReader): Set<String> {
        logger.debug { "Getting fields in PDF..." }

        val fields = pdfReader.acroFields.allFields.keys

        logger.debug { "Got fields in PDF: $fields" }
        return fields
    }

    override fun getFieldValues(filledPdfForm: InputStream): Map<String, String> {
        logger.debug { "Getting field values in PDF..." }

        val pdfReader = PdfReader(filledPdfForm)
        val fields = getFields(pdfReader)
        val fieldValues = fields.associateWith { pdfReader.acroFields.getField(it) }

        logger.debug { "Got field values in PDF: $fieldValues" }
        return fieldValues
    }
}