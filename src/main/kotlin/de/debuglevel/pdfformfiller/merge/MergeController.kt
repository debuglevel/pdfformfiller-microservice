package de.debuglevel.pdfformfiller.merge

import de.debuglevel.pdfformfiller.form.FormService
import de.debuglevel.pdfformfiller.pdf.PdfService
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.security.annotation.Secured
import io.micronaut.security.rules.SecurityRule
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import mu.KotlinLogging
import java.util.*
import java.util.concurrent.Executors

@Secured(SecurityRule.IS_ANONYMOUS)
@Controller("/merges")
class MergeController(
    private val pdfService: PdfService,
    private val formService: FormService,
    private val mergeService: MergeService,
) {
    private val logger = KotlinLogging.logger {}

    private val scope = CoroutineScope(Executors.newSingleThreadExecutor().asCoroutineDispatcher())

    /**
     * Adds a request to merge a template form with the submitted values.
     */
    @Post("/")
    fun postOne(addMergeRequest: AddMergeRequest): HttpResponse<AddMergeResponse> {
        logger.debug("Called postOne($addMergeRequest)")

        return try {
            val merge = Merge(
                id = null,
                templateFormId = addMergeRequest.templateFormId,
                mergedFormId = null,
                flatten = addMergeRequest.flatten,
            )

            val savedMerge = mergeService.add(merge)

            scope.launch {
                mergeService.merge(
                    merge = merge,
                    values = addMergeRequest.values,
                )
            }

            val mergeResponse = AddMergeResponse(savedMerge)
            HttpResponse.created(mergeResponse)
        } catch (e: FormService.InvalidPdfException) {
            HttpResponse.badRequest()
        } catch (e: FormService.FormNotFoundException) {
            HttpResponse.badRequest()
        } catch (e: Exception) {
            logger.error(e) { "Unhandled exception" }
            HttpResponse.serverError()
        }
    }

    /**
     * Gets a merge.
     * @param id ID of the merge
     */
    @Get("/{id}")
    fun getOne(id: UUID): HttpResponse<GetMergeResponse> {
        logger.debug("Called getOne($id)")
        return try {
            val getMerge = mergeService.get(id)

            HttpResponse.ok(GetMergeResponse(getMerge))
        } catch (e: MergeService.MergeNotFoundException) {
            HttpResponse.notFound()
        } catch (e: Exception) {
            logger.error(e) { "Unhandled exception" }
            HttpResponse.serverError()
        }
    }

    /**
     * Gets all merges.
     */
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/")
    fun getList(): HttpResponse<Set<GetMergeResponse>> {
        logger.debug("Called getList()")

        return try {
            val mergeResponses = mergeService.getList()
                .map { GetMergeResponse(it) }
                .toSet()
            HttpResponse.ok(mergeResponses)
        } catch (e: Exception) {
            logger.error(e) { "Unhandled exception" }
            HttpResponse.serverError()
        }
    }

    /**
     * Deletes a merge.
     *
     * CAVEAT: Does not delete their merged forms.
     */
    @Delete("/{id}")
    fun deleteOne(id: UUID): HttpResponse<Unit> {
        logger.debug("Called deleteOne($id)")
        return try {
            mergeService.delete(id)

            HttpResponse.noContent()
        } catch (e: MergeService.MergeNotFoundException) {
            HttpResponse.notFound()
        } catch (e: Exception) {
            logger.error(e) { "Unhandled exception" }
            HttpResponse.serverError()
        }
    }

    /**
     * Deletes all merges.
     *
     * CAVEAT: Does not delete their merged forms.
     */
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Delete("/")
    fun deleteAll(): HttpResponse<Unit> {
        logger.debug("Called deleteAll()")
        return try {
            mergeService.deleteAll()

            HttpResponse.noContent()
        } catch (e: Exception) {
            logger.error(e) { "Unhandled exception" }
            HttpResponse.serverError()
        }
    }
}