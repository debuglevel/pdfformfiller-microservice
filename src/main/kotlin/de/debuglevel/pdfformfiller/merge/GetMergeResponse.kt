package de.debuglevel.pdfformfiller.merge

import java.time.LocalDateTime
import java.util.*

/**
 * An existing merge request
 * @param id ID of the merge request
 * @param templateFormId ID of the template form
 * @param mergedFormId ID of the destination form
 * @param flatten Remove PDF controls on merge
 * @param createdOn When the form was created
 * @param lastModifiedOn When the form was last modified
 */
data class GetMergeResponse(
    val id: UUID,
    val templateFormId: UUID,
    val mergedFormId: UUID?,
    val flatten: Boolean,
    val createdOn: LocalDateTime,
    val lastModifiedOn: LocalDateTime
) {
    constructor(merge: Merge) : this(
        id = merge.id!!,
        templateFormId = merge.templateFormId,
        mergedFormId = merge.mergedFormId,
        flatten = merge.flatten,
        createdOn = merge.createdOn,
        lastModifiedOn = merge.lastModifiedOn,
    )
}
