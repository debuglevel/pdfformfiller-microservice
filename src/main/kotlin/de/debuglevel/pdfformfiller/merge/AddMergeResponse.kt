package de.debuglevel.pdfformfiller.merge

import java.util.*

/**
 * Response to adding a merge request
 * @param id ID of the merge request
 * @param templateFormId ID of the template form
 */
data class AddMergeResponse(
    val id: UUID,
    val templateFormId: UUID,
) {
    constructor(merge: Merge) : this(
        id = merge.id!!,
        templateFormId = merge.templateFormId,
    )
}
