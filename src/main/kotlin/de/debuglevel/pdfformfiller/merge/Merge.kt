package de.debuglevel.pdfformfiller.merge

import io.micronaut.data.annotation.DateCreated
import io.micronaut.data.annotation.DateUpdated
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

/**
 * A merge request
 */
@Entity
data class Merge(
    /**
     * ID of the merge request
     */
    @Id
    @GeneratedValue
    var id: UUID?,
    /**
     * ID of the template form to be filled
     */
    var templateFormId: UUID,
    /**
     * ID of the merged form
     */
    var mergedFormId: UUID?,
    /**
     * Remove PDF controls on merge
     */
    var flatten: Boolean,
    // TODO: the merged values should also be saved
    @DateCreated
    var createdOn: LocalDateTime = LocalDateTime.now(),
    @DateUpdated
    var lastModifiedOn: LocalDateTime = LocalDateTime.now(),
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Merge

        if (id != other.id) return false
        if (templateFormId != other.templateFormId) return false
        if (mergedFormId != other.mergedFormId) return false
        if (flatten != other.flatten) return false
        if (createdOn != other.createdOn) return false
        if (lastModifiedOn != other.lastModifiedOn) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }
}