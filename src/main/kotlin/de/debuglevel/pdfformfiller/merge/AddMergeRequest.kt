package de.debuglevel.pdfformfiller.merge

import java.util.*

/**
 * Request to merge a form with a form template
 * @param templateFormId ID of the template form
 * @param values Map of the field names and their values to be filled in
 * @param flatten Remove PDF controls on merge
 */
data class AddMergeRequest(
    val templateFormId: UUID,
    val values: Map<String, String>,
    val flatten: Boolean,
)
