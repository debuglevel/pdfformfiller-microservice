package de.debuglevel.pdfformfiller.form

/**
 * Request to add a form
 * @param pdfBase64 Base64 encoded PDF data
 */
data class AddFormRequest(
    val pdfBase64: String
)
