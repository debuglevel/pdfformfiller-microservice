package de.debuglevel.pdfformfiller.form

import java.time.LocalDateTime
import java.util.*

/**
 * An existing form
 * @param id ID of the form
 * @param pdfBase64 Base64 encoded PDF data
 * @param values Map of fields in the PDF and their values
 * @param createdOn When the form was created
 * @param lastModifiedOn When the form was last modified
 */
data class UpdateFormResponse(
    val id: UUID,
    val pdfBase64: String?, // TODO: maybe not-nullable?
    val values: Map<String, String>?, // TODO: maybe not-nullable?
    val createdOn: LocalDateTime,
    val lastModifiedOn: LocalDateTime,
) {
    constructor(
        form: Form,
        values: Map<String, String>?
    ) : this(
        id = form.id!!,
        pdfBase64 = Base64.getEncoder().encodeToString(form.pdf),
        values = values,
        createdOn = form.createdOn,
        lastModifiedOn = form.lastModifiedOn,
    )
}
