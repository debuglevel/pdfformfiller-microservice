package de.debuglevel.pdfformfiller.form

import io.micronaut.data.annotation.DateCreated
import io.micronaut.data.annotation.DateUpdated
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Lob

@Entity
data class Form(
    @Id
    @GeneratedValue
    var id: UUID?,
    @Lob
    var pdf: ByteArray,
    @DateCreated
    var createdOn: LocalDateTime = LocalDateTime.now(),
    @DateUpdated
    var lastModifiedOn: LocalDateTime = LocalDateTime.now(),
) {
    override fun toString(): String {
        return "Form(id=$id, createdOn=$createdOn, lastModifiedOn=$lastModifiedOn)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Form

        if (id != other.id) return false
        if (!pdf.contentEquals(other.pdf)) return false
        if (createdOn != other.createdOn) return false
        if (lastModifiedOn != other.lastModifiedOn) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }
}