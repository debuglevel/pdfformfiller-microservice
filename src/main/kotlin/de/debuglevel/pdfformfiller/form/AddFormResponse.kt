package de.debuglevel.pdfformfiller.form

import java.util.*

/**
 * Response when a form was added.
 * @param id ID of the created form
 */
data class AddFormResponse(
    val id: UUID,
) {
    constructor(
        form: Form,
    ) : this(
        id = form.id!!,
    )
}
