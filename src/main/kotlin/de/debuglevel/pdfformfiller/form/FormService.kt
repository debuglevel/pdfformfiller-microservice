package de.debuglevel.pdfformfiller.form

import de.debuglevel.pdfformfiller.pdf.PdfService
import mu.KotlinLogging
import java.util.*
import javax.inject.Singleton

@Singleton
class FormService(
    private val formRepository: FormRepository,
    private val pdfService: PdfService
) {
    private val logger = KotlinLogging.logger {}

    fun get(id: UUID): Form {
        logger.debug { "Getting form with ID '$id'..." }

        val form: Form = formRepository.findById(id).orElseThrow { FormNotFoundException(id) }

        logger.debug { "Got form with ID '$id': $form" }
        return form
    }

    fun getFieldValues(id: UUID): Map<String, String> {
        logger.debug { "Getting field values for ID '$id'..." }

        val form = this.get(id)
        val fieldValues = pdfService.getFieldValues(form.pdf.inputStream())

        logger.debug { "Got field values for ID '$id'" }
        return fieldValues
    }

    fun add(form: Form): Form {
        logger.debug { "Saving form '$form'..." }

        if (!pdfService.validate(form.pdf.inputStream())) {
            throw InvalidPdfException()
        }

        val addedForm = formRepository.save(form)

        logger.debug { "Saved form: $addedForm" }
        return form
    }

    fun update(id: UUID, form: Form): Form {
        logger.debug { "Updating form '$form' with ID $id..." }

        if (!pdfService.validate(form.pdf.inputStream())) {
            throw InvalidPdfException()
        }

        val existingForm = this.get(id).apply {
            pdf = form.pdf
        }

        val updatedForm = formRepository.update(existingForm)

        logger.debug { "Updated form with ID $id: $updatedForm" }
        return updatedForm
    }

    fun getList(): Set<Form> {
        logger.debug { "Getting all forms..." }

        // TODO: improve performance by not retrieving the data field (and then not using it)
        val forms = formRepository.findAll().toSet()

        logger.debug { "Got all ${forms.size} forms" }
        return forms
    }

    fun delete(id: UUID) {
        logger.debug { "Deleting form with ID '$id'..." }

        if (formRepository.existsById(id)) {
            formRepository.deleteById(id)
        } else {
            throw FormNotFoundException(id)
        }

        logger.debug { "Deleted form with ID '$id'" }
    }

    fun deleteAll() {
        logger.debug { "Deleting all forms..." }

        val countBefore = formRepository.count()
        formRepository.deleteAll()
        val countAfter = formRepository.count()

        logger.debug { "Deleted ${countBefore - countAfter} of $countBefore forms, $countAfter remaining" }
    }

    class FormNotFoundException(id: UUID) : Exception("No form found with ID '$id'")
    class InvalidPdfException : Exception("File is not a valid PDF")
}


