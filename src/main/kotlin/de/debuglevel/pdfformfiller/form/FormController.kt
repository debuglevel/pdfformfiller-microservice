package de.debuglevel.pdfformfiller.form

import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.*
import io.micronaut.security.annotation.Secured
import io.micronaut.security.rules.SecurityRule
import mu.KotlinLogging
import java.net.URI
import java.util.*

@Secured(SecurityRule.IS_ANONYMOUS)
@Controller("/forms")
class FormController(
    private val formService: FormService,
) {
    private val logger = KotlinLogging.logger {}

    /**
     * Gets all forms.
     *
     * Some attributes might not be included and have to be fetched via a GET request.
     */
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Get("/")
    fun getList(): HttpResponse<Set<GetFormResponse>> {
        logger.debug("Called getList()")

        return try {
            val getFormResponses = formService.getList()
                .map {
                    GetFormResponse(
                        it,
                        null // do not plow through all PDFs to retrieve their fields
                    ).copy(pdfBase64 = null) // do not send PDF content when requesting a list of all forms
                }
                .toSet()
            HttpResponse.ok(getFormResponses)
        } catch (e: Exception) {
            logger.error(e) { "Unhandled exception" }
            HttpResponse.serverError()
        }
    }

    /**
     * Gets a form.
     */
    @Get("/{id}")
    fun getOne(id: UUID): HttpResponse<GetFormResponse> {
        logger.debug("Called getOne($id)")
        return try {
            val getForm = formService.get(id)
            val values = formService.getFieldValues(id)

            val getFormResponse = GetFormResponse(getForm, values)
            HttpResponse.ok(getFormResponse)
        } catch (e: FormService.FormNotFoundException) {
            HttpResponse.notFound()
        } catch (e: Exception) {
            logger.error(e) { "Unhandled exception" }
            HttpResponse.serverError()
        }
    }

    /**
     * Updates a form.
     */
    @Put("/{id}")
    fun putOne(id: UUID, updateFormRequest: UpdateFormRequest): HttpResponse<UpdateFormResponse> {
        logger.debug("Called putOne($id, $updateFormRequest)")
        return try {
            val form = Form(
                id = null,
                pdf = Base64.getDecoder().decode(updateFormRequest.pdfBase64)
            )

            val updatedForm = formService.update(id, form)
            val values = formService.getFieldValues(id)

            val updateFormResponse = UpdateFormResponse(updatedForm, values)
            HttpResponse.ok(updateFormResponse)
        } catch (e: FormService.FormNotFoundException) {
            HttpResponse.notFound()
        } catch (e: FormService.InvalidPdfException) {
            HttpResponse.badRequest()
        } catch (e: Exception) {
            logger.error(e) { "Unhandled exception" }
            HttpResponse.serverError()
        }
    }

    /**
     * Adds a form.
     */
    @Post("/")
    fun postOne(addFormRequest: AddFormRequest): HttpResponse<AddFormResponse> {
        logger.debug("Called postOne($addFormRequest)")
        return try {
            val form = Form(
                id = null,
                pdf = Base64.getDecoder().decode(addFormRequest.pdfBase64)
            )

            val savedForm = formService.add(form)

            val addFormResponse = AddFormResponse(savedForm)
            HttpResponse.created(addFormResponse, URI(savedForm.id.toString()))
        } catch (e: FormService.InvalidPdfException) {
            HttpResponse.badRequest()
        } catch (e: Exception) {
            logger.error(e) { "Unhandled exception" }
            HttpResponse.serverError()
        }
    }

    /**
     * Deletes a form.
     */
    @Delete("/{id}")
    fun deleteOne(id: UUID): HttpResponse<Unit> {
        logger.debug("Called deleteOne($id)")
        return try {
            formService.delete(id)

            HttpResponse.noContent()
        } catch (e: FormService.FormNotFoundException) {
            HttpResponse.notFound()
        } catch (e: Exception) {
            logger.error(e) { "Unhandled exception" }
            HttpResponse.serverError()
        }
    }

    /**
     * Deletes all forms.
     */
    @Secured(SecurityRule.IS_AUTHENTICATED)
    @Delete("/")
    fun deleteAll(): HttpResponse<Unit> {
        logger.debug("Called deleteAll()")
        return try {
            formService.deleteAll()

            HttpResponse.noContent()
        } catch (e: Exception) {
            logger.error(e) { "Unhandled exception" }
            HttpResponse.serverError()
        }
    }
}