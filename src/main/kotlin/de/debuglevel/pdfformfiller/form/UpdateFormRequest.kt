package de.debuglevel.pdfformfiller.form

/**
 * Request to update an existing form
 * @param pdfBase64 Base64 encoded PDF data
 */
data class UpdateFormRequest(
    val pdfBase64: String
)
