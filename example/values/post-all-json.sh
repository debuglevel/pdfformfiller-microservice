for file in *.json; do
  echo "POSTing $file... "
  curl -X POST -d @"$file" -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8082/values/
  echo
done