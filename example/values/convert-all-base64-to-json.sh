for file in *.base64; do
  echo -n "Create JSON request of $file... "
  cat "$file" | jq -R '. | { pdf:. }' > "$file.json"
  echo "done"
done
