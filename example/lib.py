from typing import List
import requests
import pprint
import base64


def get_base64_from_file(path: str):
    with open(path, "rb") as file:
        return base64.b64encode(file.read()).decode("ascii")


def get_dict_from_base64(base64_: str):
    return {"pdfBase64": base64_}


def upload_form(host: str, credentials, data):
    # credentials is (user, pass)
    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
    r = requests.post(f'{host}/forms/', auth=credentials, headers=headers, json=data)
    return r.json()["id"]


def merge_form(host: str, credentials, template_form_id: str, keyvalues: List[str]):
    data = {
        "templateFormId": template_form_id,
        "values": keyvalues
    }

    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
    r = requests.post(f'{host}/merges/', auth=credentials, headers=headers, json=data)
    return r.json()["id"]


def generate_test_keyvalues(keys: List[str]):
    keyvalues = {}
    for key in keys:
        keyvalues[key] = f"äöüÄÖÜß test {key}"
    return keyvalues


def fetch_form(host: str, credentials, form_id: str):
    headers = {'Accept': 'application/json'}
    r = requests.get(f'{host}/forms/{form_id}', auth=credentials, headers=headers)
    return r.json()


def fetch_merge(host: str, credentials, merge_id: str):
    headers = {'Accept': 'application/json'}
    r = requests.get(f'{host}/merges/{merge_id}', auth=credentials, headers=headers)
    return r.json()


def fetch_form_fields(host: str, credentials, form_id: str):
    json = fetch_form(host, credentials, form_id)
    return json["values"]


def fetch_form_field_keys(host: str, credentials, form_id: str):
    json = fetch_form_fields(host, credentials, form_id)
    return json.keys()


def fetch_form_field_keyvalues(host: str, credentials, form_id: str):
    json = fetch_form_fields(host, credentials, form_id)
    return json


def download_form(host: str, credentials, form_id: str, output_file: str):
    form = fetch_form(host, credentials, form_id)
    bytes_ = base64.b64decode(form["pdfBase64"])

    open(output_file, 'wb').write(bytes_)
