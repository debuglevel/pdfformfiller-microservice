import sys
import lib
import time
import datetime


def main():
    username = "SECRET_USERNAME"
    password = "SECRET_PASSWORD"
    credentials = (username, password)
    host = "http://localhost:8080"

    file = sys.argv[1]
    print(f"File: {file} ...")

    print("Converting file to Base64...")
    base64 = lib.get_base64_from_file(file)
    # print(f"Base64: {base64}")

    print("Converting Base64 to JSON...")
    data = lib.get_dict_from_base64(base64)
    # print(f"JSON: {data}")

    print("Uploading form...")
    form_id = lib.upload_form(host, credentials, data)
    print(f"Uploaded form: {form_id}")

    print("Fetching form field keys...")
    field_keys = lib.fetch_form_field_keys(host, credentials, form_id)

    print("These are the field keys recognized in the form:")
    print("================================================")
    print(*field_keys, sep="\n")
    print("================================================")


    print("Generating test values...")
    keyvalues = lib.generate_test_keyvalues(field_keys)
    # print("Test values: $keyvalues")

    print("Merging test values into form...")
    merge_id = lib.merge_form(host, credentials, form_id, keyvalues)
    print(f"Submitted merge: {merge_id}")

    print("Waiting for service to process merge...")
    time.sleep(1)

    print("Getting merged form id...")
    merged_form_id = (lib.fetch_merge(host, credentials, merge_id))["mergedFormId"]
    print(f"Got merged form id: {merged_form_id}")

    print("Fetching merged form field values...")
    merge_keyvalues = lib.fetch_form_field_keyvalues(host, credentials, merged_form_id)

    print("These are the field keys and values recognized in the merged form:")
    print("==================================================================")
    [print(f'"{key}"="{value}"') for key, value in merge_keyvalues.items()]
    print("==================================================================")

    print("Downloading and writing merged form...")
    datetime_ = datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S")
    lib.download_form(host, credentials, merged_form_id, f"{file}.test values merged on {datetime_}.pdf")


    print("Generating test values for provided keys...")
    with open("keys.txt") as fileh:
        lines = fileh.readlines()
        field_keys = [line.rstrip() for line in lines]
    keyvalues = lib.generate_test_keyvalues(field_keys)

    print("Merging provided values into form...")
    merge_id = lib.merge_form(host, credentials, form_id, keyvalues)
    print(f"Submitted merge: {merge_id}")

    print("Waiting for service to process merge...")
    time.sleep(1)

    print("Getting merged form id...")
    merged_form_id = (lib.fetch_merge(host, credentials, merge_id))["mergedFormId"]
    print(f"Got merged form id: {merged_form_id}")

    print("Fetching merged form field values...")
    merge_keyvalues = lib.fetch_form_field_keyvalues(host, credentials, merged_form_id)

    print("These are the field keys and values recognized in the merged form:")
    print("==================================================================")
    [print(f'"{key}"="{value}"') for key, value in merge_keyvalues.items()]
    print("==================================================================")

    print("Downloading and writing merged form...")
    datetime_ = datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S")
    lib.download_form(host, credentials, merged_form_id, f"{file}.provided values merged on {datetime_}.pdf")


if __name__ == '__main__':
    # execute only if run as the entry point into the program
    main()
