for file in *.json; do
  echo "POSTing $file... "
  curl -X POST -d @"$file" -H "Content-Type: application/json" -H "Accept: application/json" http://localhost:8080/forms/
  echo
done