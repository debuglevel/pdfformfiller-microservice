for file in *.base64; do
  echo -n "Create JSON request of $file... "
  cat "$file" | jq -R '. | { pdfBase64:. }' > "$file.json"
  echo "done"
done
